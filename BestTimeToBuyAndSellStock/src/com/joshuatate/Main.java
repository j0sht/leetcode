package com.joshuatate;

public class Main {

    public static void main(String[] args) {
        //int[] prices = {7,1,5,3,6,4};
        //int[] prices = {1,2,3,4,5};
        int[] prices = {7,6,4,3,1};
        int profit = maxProfit(prices);
        System.out.printf("Profit: %d\n", profit);
    }

    private static int maxProfit(int[] prices) {
        int profit = 0;
        for (int i = 1; i < prices.length; i++) {
            int diff = prices[i] - prices[i-1];
            if (diff > 0) {
                profit += diff;
            }
        }
        return profit;
    }
}
