//
//  main.cpp
//  two_sums_fastest
//
//  Created by Joshua Tate on 2020-06-28.
//  Copyright © 2020 joshuatate. All rights reserved.
//

#include <iostream>
#include <unordered_map>
#include <vector>
using namespace::std;

/**
 Problem Description:
  - Given an array of integers, return indices of the two numbers such that they add up to a specific target.
  - You may assume that each input would have exactly one solution, and you may not use the same element twice.
 */

// My solution:
// Runtime: O(n^2) --> Brute force
// Space Efficiency: O(1)
class Solution {
public:
  vector<int> twoSum(vector<int>& nums, int target) {
    vector<int> vect;
    bool found = false;
    unsigned long size = nums.size();
    for (int i = 0; !found && i < size; i++) {
      for (int j = i+1; j < size; j++) {
        if (nums[i] + nums[j] == target) {
          vect.push_back(i);
          vect.push_back(j);
          found = true;
          break;
        }
      }
    }
    return vect;
  }
};

// One-pass solution:
// Runtime: O(n)
// Space Efficiency: O(n)
class Solution2 {
public:
  vector<int> twoSum(vector<int>& nums, int target) {
    unordered_map<int, int> prevMap;

    for (int i = 0; i < nums.size(); i++) {
      int diff = target - nums[i];

      if(prevMap.count(diff))
        return { prevMap[diff], i };
      prevMap[nums[i]] = i;
    }
    return {};
  }
};

int main(int argc, const char * argv[]) {
  // insert code here...
  vector<int> input{2, 7, 11, 15};
  Solution2 sol;
  vector<int> result = sol.twoSum(input, 9);
  cout << "Target: " << 9 << endl;
  cout << "Input: [";
  for (int i = 0; i < input.size(); i++) {
    cout << input[i];
    if (i != input.size() - 1) {
      cout << ", ";
    }
  }
  cout << "]\nResult: [";
  for (int i = 0; i < result.size(); i++) {
    cout << result[i];
    if (i != result.size() - 1) {
      cout << ", ";
    }
  }
  cout << "]\n";
  return 0;
}
