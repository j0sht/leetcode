/**
 * Write fib(n) that returns nth fibonacci number,
 * and prints the numbers in the sequence, recursively
 */
function fib(n) {
  console.log(String(0));
  if (n <= 0) {
    return 0;
  } else {
    console.log(String(1));
    return (n == 1) ? 1 : fibn(0, 1, n);
  }
}

function fibn(n1, n2, i) {
  if (i <= 1) {
    return n2;
  } else {
    console.log(String(n1+n2));
    return fibn(n2, n1+n2, i-1);
  }
}

console.log(`fib(0) == ${fib(0)}`);
console.log(`fib(1) == ${fib(1)}`);
console.log(`fib(2) == ${fib(2)}`);
console.log(`fib(10) == ${fib(10)}`);
