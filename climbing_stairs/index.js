/**
 * @param {number} n
 * @return {number}
 */
/// To reach the nth step, what could have been your previous steps?
/// (think about the step sizes)
const climbStairs = function(n) {
    const nums = [0, 1];
    for (let i = 2; i <= n; i++) {
        nums.push(nums[i-1] + nums[i-2]);
    }
    return nums[n] + nums[n-1];
};

console.log(climbStairs(2));
console.log(climbStairs(2) === 2);
console.log(climbStairs(3) === 3);
console.log(climbStairs(4))
