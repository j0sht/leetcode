const singleNumber = function(nums) {
    let val = 0;
    for (const num of nums) {
        val ^= num;
    }
    return val;
};

console.log(singleNumber([2,2,1]) === 1);
console.log(singleNumber([4,1,2,1,2]) === 4);
console.log(singleNumber([1]) === 1);
