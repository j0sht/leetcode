/**
 * @param {character[]} s
 * @return {void} Do not return anything, modify s in-place instead.
 */
const reverseString = function(s) {
    const middle = Math.floor(s.length / 2);
    for (let i = 0, j = s.length-1; i < middle; i++, j--) {
        const currentValue = s[i];
        s[i] = s[j];
        s[j] = currentValue;
    }
};

console.log("hello?");
const s1 = ['h', 'e', 'l','l','o'];
reverseString(s1);
console.log(s1);

const s2 = ["H","a","n","n","a","h"];
reverseString(s2);
console.log(s2);
