const lengthOfLongestSubstring = function(s) {
    let longest = 0;
    let sub_s = '';
    for (let i = 0; i < s.length; i++) {
        const char = s[i];
        const index = sub_s.indexOf(char);
        if (index >= 0) {
            if (sub_s.length > longest) {
                longest = sub_s.length;
            }
            sub_s = sub_s.substring(index+1);
        }
        sub_s += char;
    }
    return (sub_s.length > longest) ? sub_s.length : longest;
};
