public class ListNode {
  public var val: Int
  public var next: ListNode?
  public init() { self.val = 0; self.next = nil; }
  public init(_ val: Int) { self.val = val; self.next = nil; }
  public init(_ val: Int, _ next: ListNode?) { self.val = val; self.next = next; }

  public func display() {
    var curr: ListNode? = self
    while curr != nil {
      let val = curr!.val
      print(val, terminator: "")
      if curr?.next != nil {
        print(" -> ", terminator: "")
      }
      curr = curr?.next
    }
    print()
  }
}

class Solution {
  func addTwoNumbers(_ l1: ListNode?, _ l2: ListNode?) -> ListNode? {
    let result: ListNode? = ListNode()
    var curr = result
    var currL1 = l1
    var currL2 = l2
    var overflow = false
    while currL1 != nil || currL2 != nil || overflow {
      let val1 = currL1?.val ?? 0
      let val2 = currL2?.val ?? 0
      let num = (overflow) ? (val1 + val2 + 1) : (val1 + val2)
      let digit: Int
      if num > 9 {
        overflow = true
        digit = num % 10
      } else {
        overflow = false
        digit = num
      }
      curr?.val = digit
      currL1 = currL1?.next
      currL2 = currL2?.next
      if currL1 != nil || currL2 != nil || overflow {
        curr?.next = ListNode()
        curr = curr?.next
      }
    }
    return result
  }
}
