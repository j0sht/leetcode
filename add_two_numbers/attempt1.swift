// Failed with really large number --> Integer overflow

import Foundation

public class ListNode {
  public var val: Int
  public var next: ListNode?
  public init() { self.val = 0; self.next = nil; }
  public init(_ val: Int) { self.val = val; self.next = nil; }
  public init(_ val: Int, _ next: ListNode?) { self.val = val; self.next = next; }

  public func display() {
    var curr: ListNode? = self
    while curr != nil {
      let val = curr!.val
      print(val, terminator: "")
      if curr?.next != nil {
        print(" -> ", terminator: "")
      }
      curr = curr?.next
    }
    print()
  }
}

class Solution {
  func addTwoNumbers(_ l1: ListNode?, _ l2: ListNode?) -> ListNode? {
    var num1 = 0
    var num2 = 0

    // Get num1
    var currentNode = l1
    var i = 0
    while currentNode != nil {
      if let val = currentNode?.val {
        num1 += val * Int(pow(10.0, Double(i)))
      }
      currentNode = currentNode?.next
      i += 1
    }

    // Get num2
    i = 0
    currentNode = l2
    while currentNode != nil {
      if let val = currentNode?.val {
        num2 += val * Int(pow(10.0, Double(i)))
      }
      currentNode = currentNode?.next
      i += 1
    }

    // Build result
    var sum = num1 + num2
    let result: ListNode? = ListNode()
    var curr = result
    while sum > 0 {
      let digit = sum % 10
      curr?.val = digit
      curr?.next = ((sum / 10) > 0) ? ListNode() : nil
      curr = curr?.next
      sum /= 10
    }
    return result
  }
}