// Beat 94.10% of Swift runtimes, and 91.16% of memory usage

public class ListNode {
  public var val: Int
  public var next: ListNode?
  public init() { self.val = 0; self.next = nil; }
  public init(_ val: Int) { self.val = val; self.next = nil; }
  public init(_ val: Int, _ next: ListNode?) { self.val = val; self.next = next; }

  public func display() {
    var curr: ListNode? = self
    while curr != nil {
      let val = curr!.val
      print(val, terminator: "")
      if curr?.next != nil {
        print(" -> ", terminator: "")
      }
      curr = curr?.next
    }
    print()
  }
}

class Solution {
  func addTwoNumbers(_ l1: ListNode?, _ l2: ListNode?) -> ListNode? {
    let result: ListNode? = ListNode()
    var curr = result
    var currL1 = l1
    var currL2 = l2
    var overflow = false
    while currL1 != nil || currL2 != nil || overflow {
      let sum = (currL1?.val ?? 0) + (currL2?.val ?? 0) + (overflow ? 1 : 0)
      let val: Int
      if sum > 9 {
        overflow = true
        val = sum % 10
      } else {
        overflow = false
        val = sum
      }
      curr?.val = val
      currL1 = currL1?.next
      currL2 = currL2?.next
      if currL1 != nil || currL2 != nil || overflow {
        curr?.next = ListNode()
        curr = curr?.next
      }
    }
    return result
  }
}