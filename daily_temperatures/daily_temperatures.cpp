#include <iostream>
#include <vector>

using namespace std;

class Solution {
public:
  vector<int> dailyTemperatures(vector<int>& temperatures) {
    vector<int> result{};

    auto max_temp = *max_element(temperatures.begin(), temperatures.end());

    for (auto i = 0; i < temperatures.size()-1; i++) {
      int num_days{};

      if (temperatures[i] != max_temp) {
	if (i > 0 && temperatures[i] == temperatures[i-1]) {
	  num_days = max(0, result[i-1] - 1);
	} else {
	  for (auto j = i+1; j < temperatures.size(); j++) {
	    if (temperatures[j] > temperatures[i]) {
	      num_days = j-i;
	      break;
	    }
	  }
	}
      }

      result.push_back(num_days);
    }

    result.push_back(0);

    return result;
  }
};

void run_test(vector<int> &input, vector<int> &expected) {
  cout << "Input:\n\t";
  for (auto i : input)
    cout << i << " ";

  cout << "\nExpected:\n\t";
  for (auto i : expected)
    cout << i << " ";

  auto solution = Solution{};
  auto result = solution.dailyTemperatures(input);

  cout << "\nResult:\n\t";
  for (auto i : result)
    cout << i << " ";
  cout << endl;
}

int main() {
  auto tests = vector<vector<int>>{{73, 74, 75, 71, 69, 72, 76, 73}, {30, 40, 50, 60}, {30, 60, 90}};
  auto expected = vector<vector<int>>{{1,1,4,2,1,1,0,0},{1,1,1,0},{1,1,0}};

  for (auto i = 0; i < tests.size(); i++) {
    cout << "---------------------------------------\n"
	 << "Running test " << i+1 << endl
	 << "---------------------------------------\n";

    run_test(tests[i], expected[i]);

    cout << "---------------------------------------\n";
  }

  return 0;
}
