/**
 * @param {number[]} nums
 * @return {number}
 */
const maxSubArray = function(nums) {
    let currentSum = nums[0];
    let maxSum = currentSum;
    for (let i = 1; i < nums.length; i++) {
        const sum = nums[i] + currentSum;
        currentSum = (sum > nums[i]) ? sum : nums[i];
        maxSum = (currentSum > maxSum) ? currentSum : maxSum;
    }
    return maxSum;
};

console.log(maxSubArray([8,-19,5,-4,20]) === 21)

console.log(maxSubArray([1,2]) === 3);

console.log(maxSubArray([-2,1,-3,4,-1,2,1,-5,4]) === 6);
console.log(maxSubArray([1]) === 1);
console.log(maxSubArray([0]) === 0);
console.log(maxSubArray([-1]) === -1);
console.log(maxSubArray([-2147483647]) === -2147483647);

