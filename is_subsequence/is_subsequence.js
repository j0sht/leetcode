/**
 * @param {string} s
 * @param {string} t
 * @return {boolean}
 */
var isSubsequence = function(s, t) {
    if (s === t)
        return true;

    const tStack = new Array();
    for (let i = t.length - 1; i >= 0; i--)
        tStack.push(t[i]);

    const sStack = new Array();
    for (let i = s.length - 1; i >= 0; i--)
        sStack.push(s[i]);

    let currS;
    while (tStack.length && (currS = sStack.pop())) {
        let currT;
        let matchFound = false;

        while (!matchFound && (currT = tStack.pop()))
            matchFound = currS === currT;

        if (!matchFound)
            return false;
    }

    return sStack.length === 0;
};
