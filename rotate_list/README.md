# Rotate List
- Difficulty: **Medium**
- Link: https://leetcode.com/problems/rotate-list/

## Description
- Given the `head` of a linked list, rotate the list to the right by `k` places.

## Constraints
- Number of nodes in the list is in the range `[0, 500]`
- `-100 <= Node.val <= 100`
- `0 <= k <= 2 * 10^9`

## Example
- Input:
  - `[1,2,3,4,5]`
  - `k = 2`
- Output:
  - `[4,5,1,2,3]`
