#include <iostream>
#include <queue>

constexpr int k = 2;

using namespace std;

struct ListNode {
  int val;
  ListNode *next;
  ListNode() : val(0), next(nullptr) {}
  ListNode(int x) : val(x), next(nullptr) {}
  ListNode(int x, ListNode *next) : val(x), next(next) {}
};

class Solution {
  static constexpr int empty_val = -101;

public:
  ListNode* rotateRight(ListNode* head, int k) {
    if (!head || !(head->next))
      return head;

    auto len = 0;
    auto curr = head;
    for (; curr; curr = curr->next)
      len++;

    auto start_pos = k % len;
    auto q = queue<int>{};
    curr = head;

    for (int i = 0; i < start_pos; i++) {
      q.push(curr->val);
      curr->val = empty_val;
      curr = curr->next;
    }

    while (!q.empty()) {
      if (curr->val != empty_val)
	q.push(curr->val);

      curr->val = q.front();
      q.pop();
      curr = curr->next ? curr->next : head;
    }

    return head;
  }
};

int main() {
  auto head = new ListNode(1);
  auto curr = head;
  for (auto i = 2; i < 6; i++) {
    curr->next = new ListNode(i);
    curr = curr->next;
  }

  cout << "Input:\n\t";
  curr = head;
  while (curr) {
    cout << curr->val << " ";
    curr = curr->next;
  }
  cout << endl;

  auto result = Solution().rotateRight(head, k);

  cout << "Output:\n\t";
  curr = result;
  while (curr) {
    cout << curr->val << " ";
    curr = curr->next;
  }
  cout << endl;

  return 0;
}
