//: [Previous](@previous)
/*:
 # Running Sum of 1D Array
 - Given an array nums. We define a running sum of an array as runningSum[i] = sum(nums[0]…nums[i]).
 - Return the running sum of nums.
 */
class Solution {
    func runningSum(_ nums: [Int]) -> [Int] {
        var result = [Int]()
        result.reserveCapacity(nums.count)

        var total = 0
        for num in nums {
            total += num
            result.append(total)
        }

        return result
    }
}

Solution().runningSum([1,2,3,4])
//: [Next](@next)
