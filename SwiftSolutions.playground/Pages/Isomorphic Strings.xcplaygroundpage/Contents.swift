//: [Previous](@previous)

// Solution is correct, but Swift outputs a TLE 😭
// Exact same solution in JavaScript succeeds
class Solution {
    func isIsomorphic(_ s: String, _ t: String) -> Bool {
        guard s.count == t.count else {
            return false
        }
        guard s != t else {
            return true
        }

        var sMap = [Character:Character]()
        var tMap = [Character:Character]()

        for (i, sChar) in s.enumerated() {
            let tIndex = t.index(t.startIndex, offsetBy: i)
            let tChar = t[tIndex]

            if let expected = sMap[sChar], tChar != expected {
                return false
            } else if let expected = tMap[tChar], sChar != expected {
                return false
            } else {
                sMap[sChar] = tChar
                tMap[tChar] = sChar
            }
        }

        return true
    }
}


Solution().isIsomorphic("egg", "add") == true
Solution().isIsomorphic("foo", "bar") == false
Solution().isIsomorphic("paper", "title") == true
Solution().isIsomorphic("badc", "baba") == false
//: [Next](@next)
