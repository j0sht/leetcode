//: [Move Zeroes](@previous)
/*:
 # Two Sum
 - Given an array of integers ```nums``` and an integer ```target```, return indices of the two numbers
    such that they add up to ```target```
 - You may assume that each input would have exactly one solution, and you may not use the same element twice
 - You can return the answer in any order
 - Example 1)
    - Input: ```nums = [2,7,11,15], target = 9```
    - Output: ```[0,1]``` (because ```nums[0] + nums[1] == 9```)
 - Example 2)
    - Input: ```nums = [3,2,4], target = 6```
    - Output: ```[1,2]```
 - Example 3)
    - Input: ```nums = [3,3], target = 6```
    - Output: ```[0,1]```
 - Example 4)
    - Input: ```nums = [0,4,3,0], target = 0```
    - Output: ```[0,3]```
 - Example 5)
    - Input: ```nums = [-3,4,3,90], target = 0```
    - Output: ```[0,2]```
 */
class Solution {
    func twoSumVersion1(_ nums: [Int], _ target: Int) -> [Int] {
        // Original solution (O(N^2))
        let sorted = nums.enumerated().sorted { first, second in
            first.element < second.element
        }
        for i in 0..<sorted.count {
            let curr = sorted[i]
            for j in i+1..<sorted.count {
                let other = sorted[j]
                if curr.element + other.element > target {
                    break
                } else if curr.element + other.element == target {
                    return [curr.offset, other.offset]
                }
            }
        }
        return []
    }

    func twoSum(_ nums: [Int], _ target: Int) -> [Int] {
        // Dynamic programming solution
        var prevValues = [Int:Int]()
        for (i, num) in nums.enumerated() {
            let complement = target - num
            if let prevIndex = prevValues[complement] {
                return [prevIndex, i]
            } else {
                prevValues[num] = i
            }
        }
        return []
    }
}

var nums = [2,7,11,15]
var target = 9
Solution().twoSum(nums, target) == [0,1]

nums = [3,2,4]
target = 6
Solution().twoSum(nums, target) == [1,2]

nums = [3,3]
target = 6
Solution().twoSum(nums, target) == [0,1]

nums = [0,4,3,0]
target = 0
Solution().twoSum(nums, target) == [0,3]

nums = [-3,4,3,90]
target = 0
Solution().twoSum(nums, target) == [0,2]
//: [Valid Sudoku](@next)
