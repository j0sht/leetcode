//: [Two Sum](@previous)
/*:
 # Valid Sudoku
 - Determine if a `9 x 9` Sudoku board is valid.
 - Only the filled cells need to be validated.
 */
class Solution {
    func isValidSudoku(_ board: [[Character]]) -> Bool {
        // Check rows
        for row in board {
            let numbers = row.compactMap { Int(String($0)) }
            if numbers.count != Set(numbers).count {
                return false
            }
        }
        // Check columns
        for colIndex in 0..<9 {
            let values = (0..<9).map { board[$0][colIndex] }
            let numbers = values.compactMap { Int(String($0)) }
            if numbers.count != Set(numbers).count {
                return false
            }
        }
        // Check grids
        var grids = [[Character]](repeating: [], count: 9)
        for (index, row) in board.enumerated() {
            let gridIndices: (Int, Int, Int)
            switch index {
            case 0..<3:
                gridIndices = (0,1,2)
            case 3..<6:
                gridIndices = (3,4,5)
            default:
                gridIndices = (6,7,8)
            }
            row[0..<3].forEach { grids[gridIndices.0].append($0) }
            row[3..<6].forEach { grids[gridIndices.1].append($0) }
            row[6..<9].forEach { grids[gridIndices.2].append($0) }
        }
        for grid in grids {
            let numbers = grid.compactMap { Int(String($0)) }
            if numbers.count != Set(numbers).count {
                return false
            }
        }
        return true
    }
}

let validBoard: [[Character]] = [["5","3",".",".","7",".",".",".","."]
                                 ,["6",".",".","1","9","5",".",".","."]
                                 ,[".","9","8",".",".",".",".","6","."]
                                 ,["8",".",".",".","6",".",".",".","3"]
                                 ,["4",".",".","8",".","3",".",".","1"]
                                 ,["7",".",".",".","2",".",".",".","6"]
                                 ,[".","6",".",".",".",".","2","8","."]
                                 ,[".",".",".","4","1","9",".",".","5"]
                                 ,[".",".",".",".","8",".",".","7","9"]]

let invalidBoard: [[Character]] = [["8","3",".",".","7",".",".",".","."]
                                   ,["6",".",".","1","9","5",".",".","."]
                                   ,[".","9","8",".",".",".",".","6","."]
                                   ,["8",".",".",".","6",".",".",".","3"]
                                   ,["4",".",".","8",".","3",".",".","1"]
                                   ,["7",".",".",".","2",".",".",".","6"]
                                   ,[".","6",".",".",".",".","2","8","."]
                                   ,[".",".",".","4","1","9",".",".","5"]
                                   ,[".",".",".",".","8",".",".","7","9"]]


Solution().isValidSudoku(validBoard)
Solution().isValidSudoku(invalidBoard)
//: [Next](@next)
