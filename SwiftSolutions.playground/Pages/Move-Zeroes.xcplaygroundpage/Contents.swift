/*:
 # Move Zeroes
 - Given an array of ```nums```, move all the ```0```'s to the end of it while maintaining the relative order of the non-zero elements
 - Example 1)
    -  Input: ```nums = [0,1,0,3,12]```
    -  Output: ```[1,3,12,0,0]```
 - Example 2)
    - Input: ```nums = [0]```
    - Output: ```[0]```
 */
class Solution {
    func moveZeroes(_ nums: inout [Int]) {
        let nonZeroes = nums.filter { $0 != 0 }
        for (i, num) in nonZeroes.enumerated() {
            nums[i] = num
        }
        for i in nonZeroes.count..<nums.count {
            nums[i] = 0
        }
    }
}

var nums = [0,1,0,3,12]
Solution().moveZeroes(&nums)

nums = [0]
Solution().moveZeroes(&nums)
//: [Two Sum](@next)
