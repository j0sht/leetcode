const factorial = (n) => {
    let result = 1;
    for (let i = n; i > 0; i--) {
        result *= i;
    }
    return result;
};

/**
 * @param {number[]} nums
 * @return {number[][]}
 */
const permute = function(nums) {
    const possibilities = factorial(nums.length);
    const result = new Array(possibilities);
    result.fill((new Array(nums.length)).fill(undefined));
    for (let i = 0; i < nums.length; i++) {
        const currentValue = nums[i];

    }
    return result;
};


console.log(factorial(3));
console.log(permute([1,2,3]));
