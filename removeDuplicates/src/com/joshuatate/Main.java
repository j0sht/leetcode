package com.joshuatate;

public class Main {

    public static void main(String[] args) {
	    int[] nums = new int[]{0, 0, 1, 1, 1, 2, 2, 3, 3, 4};
        //int[] nums = new int[]{1,1,2};
        //int[] nums = new int[]{1,2};
        //int[] nums = new int[]{1,1};
        //int[] nums = new int[]{1,2,3};
	    int k = removeDuplicates(nums);
	    System.out.println(k);
    }

    public static int removeDuplicates(int[] nums) {
        int k = 0;
        for (int i = 0; i < nums.length; i++) {
            int nextIndex = i+1;
            int j = nextIndex;
            while (j < nums.length && (nums[j] == nums[i] || nums[i] > nums[j])) {
                j += 1;
            }
            k++;
            if (j < nums.length && nextIndex != j) {
                nums[nextIndex] = nums[j];
            } else {
                if (j < nums.length) {
                    continue;
                }
                break;
            }
        }
        return k;
    }

    public static int removeDuplicatesSolution(int[] nums) {
        if (nums.length == 0) {
            return 0;
        }
        int i = 0;
        for (int j = 1; j < nums.length; j++) {
            if (nums[j] != nums[i]) {
                i++;
                nums[i] = nums[j];
            }
        }
        return i + 1;
    }
}
