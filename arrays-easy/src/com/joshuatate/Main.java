package com.joshuatate;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
        testRotate();
    }

    // Given an array and an int k, rotate the array
    // to the right by k steps
    // My solution:
    // Time: O(n)
    // Space: O(n)
    private static void rotate(int[] nums, int k) {
        Set<Integer> seen = new HashSet<>();
        int currPos = 0;
        int curr = nums[currPos];
        while (seen.size() != nums.length) {
            if (seen.contains(currPos)) {
                currPos++;
                curr = nums[currPos];
            }
            seen.add(currPos);
            int nextPos = (currPos + k) % nums.length;
            int next = nums[nextPos];
            nums[nextPos] = curr;
            curr = next;
            currPos = nextPos;
        }
    }

    // Rotate with reverse:
    // Time: O(n)
    // Space: O(1)
    private static void rotateWithReverse(int[] nums, int k) {
        k %= nums.length;
        reverse(nums, 0, nums.length-1);
        reverse(nums, 0, k-1);
        reverse(nums, k, nums.length-1);
    }

    private static void reverse(int[] nums, int start, int end) {
        while (start < end) {
            int temp = nums[end];
            nums[end] = nums[start];
            nums[start] = temp;
            start++;
            end--;
        }
    }

    private static void testRotate() {
        int[] nums = {1,2,3,4,5,6,7};
        int k = 3;
        rotateWithReverse(nums, k);
        boolean result = Arrays.equals(nums, new int[]{5, 6, 7, 1, 2, 3, 4});
        System.out.println(result);

        nums = new int[]{-1, -100, 3, 99};
        k = 2;
        rotateWithReverse(nums, k);
        result = Arrays.equals(nums, new int[]{3,99,-1,-100});
        System.out.println(result);
    }
}
