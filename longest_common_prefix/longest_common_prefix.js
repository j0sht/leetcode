/**
 * @param {string[]} strs
 * @return {string}
 */
const longestCommonPrefix = function(strs) {
    let result = '';
    while (strs.length > 0) {
        const letters = strs.map((s) => s[0] ? s[0] : '');
        const allSame = letters.reduce((acc, curr) => acc && curr === letters[0], true);
        if (allSame && letters[0] !== '') {
            result += letters[0];
            strs = strs.map((s) => s.slice(1));
        } else {
            break;
        }
    }
    return result;
}

console.log(longestCommonPrefix(["flower","flow","flight"]) === "fl");
console.log(longestCommonPrefix(["dog","racecar","car"]) === "");
