const getMedian = (arr) => {
    if ((arr.length % 2) === 0) {
        const mid = arr.length / 2;
        const val1 = arr[mid-1];
        const val2 = arr[mid];
        return (val1 + val2) / 2;
    } else {
        const mid = Math.floor(arr.length / 2);
        return arr[mid];
    }
};

/**
 * @param {number[]} nums1
 * @param {number[]} nums2
 * @return {number}
 */
const findMedianSortedArrays = function(nums1, nums2) {
    if (nums1.length === 1 && nums2.length === 1) {
        const val1 = nums1[0];
        const val2 = nums2[0];
        return (val1 + val2) / 2;
    } else if (nums1.length === 0) {
        return getMedian(nums2);
    } else if (nums2.length === 0) {
        return getMedian(nums1);
    }

    if (nums1[0] < nums2[0] && nums1[nums1.length-1] > nums2[nums2.length-1]) {
        return getMedian(nums2);
    } else if (nums2[0] < nums1[0] && nums2[nums2.length-1] > nums1[nums1.length-1]) {
        return getMedian(nums1);
    }

    const median1 = getMedian(nums1);
    const median2 = getMedian(nums2);
    if (median1 === median2) {
        return median1;
    } else if (median1 > median2) {
        const firstHalf = nums1.slice(0, Math.floor(nums1.length / 2));
        const secondHalf = nums2.slice(Math.floor(nums2.length / 2), nums2.length+1);
        return findMedianSortedArrays(firstHalf, secondHalf);
    } else {
        const firstHalf = nums2.slice(0, Math.floor(nums2.length / 2));
        const secondHalf = nums1.slice(Math.floor(nums1.length / 2), nums1.length+1);
        return findMedianSortedArrays(firstHalf, secondHalf);
    }
};

// console.log(merge([1,2], [1,2,3]));

// console.log(findMedianSortedArrays([1,3], [2,7]) === 2.5);
// console.log(findMedianSortedArrays([1,2], [1,2,3]) === 2);
// console.log(findMedianSortedArrays([1,2], [-1, 3]) === 1.5);
console.log(findMedianSortedArrays([1,3], [2]) === 2);
console.log(findMedianSortedArrays([1,2], [3,4]) === 2.5);
console.log(findMedianSortedArrays([0,0], [0,0]) === 0);
console.log(findMedianSortedArrays([], [1]) === 1);
console.log(findMedianSortedArrays([2], []) === 2);

