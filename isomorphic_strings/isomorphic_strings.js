/**
 * @param {string} s
 * @param {string} t
 * @return {boolean}
 */
var isIsomorphic = function(s, t) {
    if (s.length !== t.length)
        return false;

    const sMap = new Object();
    const tMap = new Object();

    for (let i = 0; i < s.length; i++) {
        const sChar = s[i];
        const tChar = t[i];

        const sValue = sMap[sChar];
        const tValue = tMap[tChar];

        if (sValue && sValue !== tChar)
            return false;
        if (tValue && tValue !== sChar)
            return false;

        sMap[sChar] = tChar;
        tMap[tChar] = sChar;
    }

    return true;
};
